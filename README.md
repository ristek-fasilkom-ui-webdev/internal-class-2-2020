# Installation

## Installation Backend
1. Asumsi kalian sudah familiar dengan Django. Bila belum, silahkan baca tutorial Django sampai part 2
    - https://docs.djangoproject.com/en/3.0/intro/tutorial01/
2. Install python3
3. Install pip3 dan venv
    - Untuk Windows biasanya sudah terinstall saat install python
    - Untuk Linux (Ubuntu), bisa dilakukan dengan command berikut
        - `sudo apt get python3-pip`
        - `sudo apt get python3-venv`
4. Membuat virtual environment Python di intro_to_backend_python
    - Masuk ke folder intro_to_backend_python
    - Buat virtualenv dengan command berikut
        - Untuk Linux, `python3 -m venv env`
        - Untuk Windows, `py -m venv env`
5. Install requirements (packages) untuk Python
    - Aktifkan virtual environment (asumsi kalian ada di folder intro_to_backend_python)
        - Untuk Linux, `source env/bin/activate`
        - Untuk Windows (Powershell), `env\Scripts\Activate.ps1`
        - Untuk Windows (cmd), `env\Scripts\activate.bat`
    - Jalankan command `pip install -r requirements.txt`
6. Lakukan migration database dengan command `python manage.py migrate`
7. Buat sebuah superuser dengan command `python manage.py createsuperuser`
8. Backend server bisa dijalankan dengan command `python manage.py runserver 5000`

## Installation Frontend
1. Asumsi kalian telah menginstall npm atau yarn
2. Masuk ke folder intro_to_backend_react
3. Jalankan command `yarn install` atau `npm install`
4. Frontend bisa dijalankan dengan command `yarn start` atau `npm start`