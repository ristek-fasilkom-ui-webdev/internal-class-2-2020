from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from api.models import Todo
from api.serializers import TodoSerializer

class TodoView(APIView):
    def get(self, request):
        todos = Todo.objects.all()
        serialized_todos = TodoSerializer(todos, many=True)

        return Response(serialized_todos.data, status=status.HTTP_200_OK)
    
    def post(self, request):
        data = request.data
        data['is_done'] = False
        deserialized_data = TodoSerializer(data=data)
        if deserialized_data.is_valid():
            todo = deserialized_data.save()
            serialized_data = TodoSerializer(todo)
            return Response(serialized_data.data, status=status.HTTP_201_CREATED)
        return Response({'error': 'Data tidak valid'}, status=status.HTTP_400_BAD_REQUEST)

