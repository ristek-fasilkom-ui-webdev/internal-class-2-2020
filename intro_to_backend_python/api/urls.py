from django.urls import re_path
from api import views

urlpatterns = [
    re_path(r'^todo$', views.TodoView.as_view()),
]
