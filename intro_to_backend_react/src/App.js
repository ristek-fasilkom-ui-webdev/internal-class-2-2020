import React from 'react';
import ListTodo from './components/ListTodo';
import CreateTodo from './components/CreateTodo';

function App() {
  return (
    <div className="App">
      <ListTodo />
      <CreateTodo />
    </div>
  );
}

export default App;
