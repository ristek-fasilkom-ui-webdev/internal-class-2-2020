import React from 'react';
import axios from 'axios';
import './index.css';

class ListTodo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listTodo: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:5000/api/todo')
      .then(response => {
        const { data } = response; // const data = response.data
        this.setState({
          ...this.state,
          listTodo: data
        }); 
      });
  }

  render() {
    return (
      <>
        {this.state.listTodo.map(todo => (
          <div className="card">
            <p>{todo.title}</p>
            <p>{todo.description}</p>
            <p>{todo.is_done}</p>
          </div>
        ))}
      </>
    );
  }
}

export default ListTodo;