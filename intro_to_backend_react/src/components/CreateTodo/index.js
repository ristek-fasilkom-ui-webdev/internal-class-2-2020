import React from 'react';
import axios from 'axios';

class CreateTodo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: ''
    }
  }

  onChange(fieldName, fieldValue) {
    if (fieldName === 'title') {
      this.setState({
        title: fieldValue
      });
    } else if (fieldName === 'description') {
      this.setState({
        description: fieldValue
      });
    }
  }

  handlePost() {
    const formData = {
      title: this.state.title,
      description: this.state.description
    }
    axios.post('http://localhost:5000/api/todo', formData)
      .then(response => {
        // Reminder kalo kalian ngoding beneran plz don't do this console and alert thing.
        console.log(response);
        alert('POST berhasil');
      })
      .catch(err => {
        console.log(err);
        alert('POST gagal');
      });
  }

  render() {
    const { title, description } = this.state;
    return (
      <>
        <form>
          <input
            type="text"
            name="title"
            value={title}
            onChange={event => this.onChange('title', event.target.value)}
          />
          <input
            type="text"
            name="description"
            value={description}
            onChange={event => this.onChange('description', event.target.value)}
          />
        </form>
        <button  onClick={() => this.handlePost()}>Tambah Todo Baru</button>
      </>
    );
  }
}

export default CreateTodo;
